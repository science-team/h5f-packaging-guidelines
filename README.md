# HDF5 filter plugin packaging guidelines


# Naming conventions

A good default name for a package containing an HDF5 filter plugin is
`hdf5-filter-plugin-*-{serial,openmpi}` which matches how HDF5 documentation
refers to them.

For packages that ship multiple plugins, or that ship plugins and other code,
you can `Provide:` one or more `hdf5-filter-plugin-*-{serial,openmpi}` virtual
package names, so that packages requiring the plugin can depend on it, however
it is being distributed.


# openmpi versions of plugins

You are free to package the two versions in two different packages, or in a
single one.

If you bundle them in a single package, you can use virtual package names to
show that it contains both versions.


# testing openmpi versions of plugins

This is a simple way of testing that the openmpi version of a plugin loads
correctly:

```
H5PY_ALWAYS_USE_MPI=1 python3
>>> import h5py
>>> h5py.h5z.filter_avail(<code>)
```

You can use it in `debian/tests/control` to have autopkgtests check that
plugins load correctly:

```
Test-Command: python3 -c 'import sys, h5py; sys.exit(not h5py.h5z.filter_avail(32008))'
Depends: python3, python3-h5py, @, @recommends!
Restrictions: allow-stderr

Test-Command: H5PY_ALWAYS_USE_MPI=1 python3 -c 'import sys, h5py; sys.exit(not h5py.h5z.filter_avail(32008))'
Depends: python3, python3-h5py, @, @recommends!
Restrictions: allow-stderr
```

This example uses 32008 as an example filter plugin ID, which is the ID for
bitshuffle: remember to change it with the one(s) for your own package!
